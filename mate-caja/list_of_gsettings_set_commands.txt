--------------------------------------
dconf-editor
--------------------------------------
/org/mate/desktop/media-handling/
--------------------------------------
automount false

automount-open false
--------------------------------------
/org/gnome/desktop/interface/
--------------------------------------
cursor-blink false

enable-animations false

enable-hot-corners false

font-antialiasing 'none'

font-hinting 'none'

gtk-theme Breeze-Dark

icon-theme Papirus-Dark
--------------------------------------
/org/mate/caja/desktop/
--------------------------------------
computer-icon-visible false

home-icon-visible false

volumes-visible false
--------------------------------------
/org/mate/desktop/background/
--------------------------------------
background-fade false

draw-background false
--------------------------------------
/org/mate/desktop/interface/
--------------------------------------
automatic-mnemonics false

cursor-blink false

enable-animations false

gtk-enable-animations false
--------------------------------------
/org/mate/caja/preferences/
--------------------------------------
mouse-use-extra-buttons false

show-notifications false
--------------------------------------
