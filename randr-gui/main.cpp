#include <QApplication>
#include <QWidget>
#include <QComboBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QProcess>
#include <QStringList>
#include <QDebug>
#include <QPalette>

class ResolutionChanger : public QWidget {
    Q_OBJECT

public:
    // Constructor for the ResolutionChanger class
    ResolutionChanger(QWidget *parent = nullptr) : QWidget(parent) {
        // Initialize the user interface
        initUI();

        // Populate the list of resolutions
        populateResolutions();

        // Connect signals to slots
        connect(changeButton, &QPushButton::clicked, this, &ResolutionChanger::changeResolution);
    }

private slots:
    // Slot function to handle the resolution change button click
    void changeResolution() {
        QString resolution = resolutionComboBox->currentText();
        QString output = outputComboBox->currentText();
        qDebug() << "Changing resolution to:" << resolution << "on output:" << output;

        // Execute the wlr-randr command to change the resolution
        QProcess process;
        QProcessEnvironment environment = QProcessEnvironment::systemEnvironment();
        environment.insert("WAYLAND_DISPLAY", "wayland-0");

        process.setProcessEnvironment(environment);
        process.start("wlr-randr", QStringList() << "--output" << output << "--custom-mode" << resolution);
        process.waitForFinished();

        if (process.exitCode() == 0) {
            qDebug() << "Resolution changed successfully.";
        } else {
            qDebug() << "Error changing resolution:" << process.errorString();
        }
    }

private:
    // Function to initialize the user interface
    void initUI() {
        // Create combo boxes and buttons
        resolutionComboBox = new QComboBox;
        changeButton = new QPushButton("Change Resolution");

        outputComboBox = new QComboBox;
        outputComboBox->addItem("VGA-1");
        outputComboBox->addItem("LVDS-1");

        // Create a vertical layout to organize the widgets
        QVBoxLayout *layout = new QVBoxLayout;
        layout->addWidget(outputComboBox);
        layout->addWidget(resolutionComboBox);
        layout->addWidget(changeButton);

        // Set the layout for the widget
        setLayout(layout);
        setWindowTitle("Monitor");

        // Set a black theme using Fusion
        QPalette darkPalette;
        darkPalette.setColor(QPalette::Window, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::WindowText, Qt::white);
        darkPalette.setColor(QPalette::Base, QColor(25, 25, 25));
        darkPalette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
        darkPalette.setColor(QPalette::ToolTipText, Qt::white);
        darkPalette.setColor(QPalette::Text, Qt::white);
        darkPalette.setColor(QPalette::Button, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::ButtonText, Qt::white);
        darkPalette.setColor(QPalette::BrightText, Qt::red);
        darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));
        darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
        darkPalette.setColor(QPalette::HighlightedText, Qt::black);
        qApp->setPalette(darkPalette);
    }

    // Function to populate the list of resolutions
    void populateResolutions() {
        // Add some example resolutions
        resolutionComboBox->addItem("640x480");
        resolutionComboBox->addItem("800x600");
        resolutionComboBox->addItem("1024x768");
        resolutionComboBox->addItem("1280x720");
        resolutionComboBox->addItem("1280x1024");
        resolutionComboBox->addItem("1360x768");
        resolutionComboBox->addItem("1366x768");
        resolutionComboBox->addItem("1440x900");
        resolutionComboBox->addItem("1600x900");
        resolutionComboBox->addItem("1600x1200");
        resolutionComboBox->addItem("1920x1080");
        // Add more resolutions as needed
    }

    // Member variables for UI components
    QComboBox *outputComboBox;
    QComboBox *resolutionComboBox;
    QPushButton *changeButton;
};

// Main function
int main(int argc, char *argv[]) {
    // Create the application object
    QApplication a(argc, argv);

    // Create an instance of the ResolutionChanger class
    ResolutionChanger resolutionChanger;
    
    // Show the main window
    resolutionChanger.show();

    // Run the application event loop
    return a.exec();
}

#include "main.moc"
