#include <QApplication>
#include <QMainWindow>
#include <QPushButton>
#include <QHBoxLayout>
#include <QProcess>
#include <QPalette>
#include <QColor>

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    // Constructor for the MainWindow class
    MainWindow(QWidget *parent = nullptr) : QMainWindow(parent) {
        // Dark color palette configuration
        QPalette darkPalette;
        darkPalette.setColor(QPalette::Window, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::WindowText, Qt::white);
        darkPalette.setColor(QPalette::Base, QColor(25, 25, 25));
        darkPalette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
        darkPalette.setColor(QPalette::ToolTipText, Qt::white);
        darkPalette.setColor(QPalette::Text, Qt::white);
        darkPalette.setColor(QPalette::Button, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::ButtonText, Qt::white);
        darkPalette.setColor(QPalette::BrightText, Qt::red);
        darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));
        darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
        darkPalette.setColor(QPalette::HighlightedText, Qt::black);

        // Apply the dark color palette to the application
        qApp->setPalette(darkPalette);

        // Creation of the "Monitor" button
        QPushButton *monitorButton = new QPushButton("Monitor", this);
        connect(monitorButton, &QPushButton::clicked, this, &MainWindow::openMonitor);

        // Creation of the "Bluetooth" button
        QPushButton *bluetoothButton = new QPushButton("Bluetooth", this);
        connect(bluetoothButton, &QPushButton::clicked, this, &MainWindow::openBluetooth); // Connect to openBluetooth slot

        // Layout to organize the buttons horizontally
        QHBoxLayout *layout = new QHBoxLayout;
        layout->addWidget(monitorButton);
        layout->addWidget(bluetoothButton);

        // Central widget for the window
        QWidget *centralWidget = new QWidget;
        centralWidget->setLayout(layout);

        // Set the central widget for the window
        setCentralWidget(centralWidget);
    }

private slots:
    // Slot function to open the monitor program
    void openMonitor() {
        // Path of the program to be opened
        QString programa = "/usr/bin/resolution";

        // Open the program using QProcess
        QProcess *process = new QProcess(this);
        process->start(programa);
        process->waitForFinished();
    }

    // Slot function to open the Bluetooth program
    void openBluetooth() {
        // Path of the program to be opened
        QString programa = "/usr/bin/resolution"; // Change to the correct path for the Bluetooth program, if necessary

        // Open the program using QProcess
        QProcess *process = new QProcess(this);
        process->start(programa);
        process->waitForFinished();
    }
};

// Main function
int main(int argc, char *argv[]) {
    // Create the application object
    QApplication app(argc, argv);

    // Create an instance of the MainWindow class
    MainWindow mainWindow;

    // Show the main window
    mainWindow.show();

    // Run the application event loop
    return app.exec();
}

#include "main.moc"
