#!/bin/bash

clear

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

clear

echo "#################################################################"
echo "(/usr/share/doc/labwc)"
echo "#################################################################"

rm -rf ~/.config/sfwbar

rm -rf ~/.config/labwc

rm -rf ~/.config/gtk-3.0

sudo rm -rf /usr/share/applications/poweroff.desktop

sudo rm -rf /usr/share/applications/print-labwc.desktop

sudo rm -rf /usr/share/applications/reboot.desktop

sudo rm -rf /usr/share/applications/labwc-screenshot-sfwbar.desktop

sudo rm -rf /usr/share/applications/labwc-settings.desktop

sudo rm -rf /usr/share/backgrounds/labwc

sudo rm -rf /usr/share/themes/Onyx

sudo rm -rf /usr/share/themes/themelabwc

sudo rm -rf /usr/bin/labwc-print

sudo rm -rf /usr/bin/labwc-screenshot

sudo rm -rf /usr/bin/mini-opti

sudo rm -rf /usr/bin/opti-sway-i3wm-mate-labwc

sudo rm -rf /usr/bin/my-config-sfwbar-version

sudo rm -rf /usr/bin/caja-tweaks

sudo rm -rf /usr/bin/update-mesa-git-gitlab

mkdir -p ~/.config/sfwbar

mkdir -p ~/.config/labwc

mkdir -p ~/.config/gtk-3.0

sudo mkdir -p /usr/share/backgrounds/labwc

cp -r /usr/share/sfwbar/* ~/.config/sfwbar/

clear

echo "#################################################################"
echo "(SFWBAR/LABWC)"
echo "#################################################################"

cp sfwbar-custom.config ~/.config/sfwbar/

cp pipewire-module.widget ~/.config/sfwbar/

cp startmenu.widget ~/.config/sfwbar/

sudo cp poweroff.desktop /usr/share/applications/

sudo cp print-labwc.desktop /usr/share/applications/

sudo cp reboot.desktop /usr/share/applications/

sudo cp labwc-screenshot-sfwbar.desktop /usr/share/applications/

sudo cp labwc-settings.desktop /usr/share/applications/

sudo cp -r LABWC/themelabwc /usr/share/themes/themelabwc

cp gtk-3.0/settings.ini ~/.config/gtk-3.0/

cp LABWC/rc.xml ~/.config/labwc/

cp LABWC/autostart ~/.config/labwc/

sudo cp LABWC/labwc-wallpaper.png /usr/share/backgrounds/labwc/

sudo cp waylandlogo.png /usr/share/backgrounds/labwc/

sudo cp LABWC/labwc-print /usr/bin/

sudo cp LABWC/labwc-screenshot /usr/bin/

sudo cp LABWC/mini-opti /usr/bin/

sudo cp LABWC/opti-sway-i3wm-mate-labwc /usr/bin/

sudo cp my-config-sfwbar-version /usr/bin/

sudo cp mate-caja/caja-tweaks /usr/bin/

sudo cp update-mesa-git-gitlab /usr/bin/

sudo chmod +x /usr/bin/labwc-print

sudo chmod +x /usr/bin/labwc-screenshot

sudo chmod +x /usr/bin/mini-opti

sudo chmod +x /usr/bin/opti-sway-i3wm-mate-labwc

sudo chmod +x /usr/bin/my-config-sfwbar-version

sudo chmod +x /usr/bin/caja-tweaks

sudo chmod +x /usr/bin/update-mesa-git-gitlab

clear

sudo pacman -Rdd mesa-git-gitlab

sudo pacman -Rns mesa-git-gitlab

sudo pacman -Rdd lib32-mesa-git-gitlab

sudo pacman -Rns lib32-mesa-git-gitlab

sudo pacman -Rns cage-github

sudo pacman -Rns swaybg

sudo pacman -Rns caja

sudo pacman -Rns caja-github

sudo pacman -Rdd mate-desktop

sudo pacman -Rns mate-desktop

sudo pacman -Rns wlr-randr-sourcehut

sudo pacman -Rns labwc-github

sudo pacman -Rns labwc

sudo pacman -Rns sfwbar-github

sudo pacman -Rdd wlroots

sudo pacman -Rns wlroots

sudo pacman -Rdd wlroots0.16

sudo pacman -Rns wlroots0.16

sudo pacman -Rdd xorg-xwayland

sudo pacman -Rns xorg-xwayland

sudo pacman -Rdd wayland

sudo pacman -Rns wayland

clear

dconf write /org/mate/desktop/background/picture-filename "''"

clear

echo "#################################################################"
echo "(wayland-gitlab)"
echo "#################################################################"

clear

cd wayland-gitlab

makepkg -sirc

cd ..

clear

echo "#################################################################"
echo "(wlr-randr-sourcehut)"
echo "#################################################################"

clear

cd wlr-randr-sourcehut

makepkg -sirc

cd ..

clear

echo "#################################################################"
echo "(labwc-github)"
echo "#################################################################"

clear

cd labwc-github

makepkg -sirc

cd ..

clear

echo "#################################################################"
echo "(sfwbar-github)"
echo "#################################################################"

clear

cd sfwbar-github

makepkg -sirc

cd ..

clear

echo "#################################################################"
echo "(xwayland/swaybg)"
echo "#################################################################"

clear

sudo pacman -S swaybg nettle libepoxy libxfont2 pixman xorg-server-common libxcvt libglvnd libxau libdrm libtirpc libxshmfence libdecor libei xorg-xwayland

clear

echo "#################################################################"
echo "(mate-desktop-github)"
echo "#################################################################"

clear

cd mate-desktop-github

makepkg -sirc

cd ..

clear

echo "#################################################################"
echo "(caja-github)"
echo "#################################################################"

clear

cd caja-github

makepkg -sirc

cd ..

clear

echo "#################################################################"
echo "(randr-gui)"
echo "#################################################################"

clear

sudo rm -rf /usr/bin/resolution

clear

cd randr-gui

export CFLAGS="-march=native -Ofast -pipe"
export CXXFLAGS="${CFLAGS}"
export LDFLAGS="-Wl,--as-needed -Wl,-Ofast ${CFLAGS}"

qmake6

make -j$(nproc)

clear

sudo cp resolution /usr/bin/

sudo chmod +x /usr/bin/resolution

cd ..

clear

echo "#################################################################"
echo "(labwc-settings)"
echo "#################################################################"

clear

sudo rm -rf /usr/bin/labwc-settings

clear

cd labwc-settings

export CFLAGS="-march=native -Ofast -pipe"
export CXXFLAGS="${CFLAGS}"
export LDFLAGS="-Wl,--as-needed -Wl,-Ofast ${CFLAGS}"

qmake6

make -j$(nproc)

clear

sudo cp labwc-settings /usr/bin/

sudo chmod +x /usr/bin/labwc-settings

cd ..

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
